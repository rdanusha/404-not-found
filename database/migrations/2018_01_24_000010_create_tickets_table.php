<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tickets';

    /**
     * Run the migrations.
     * @table tickets
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ticket_no', 45)->nullable();
            $table->string('subject', 100)->nullable();
            $table->text('details')->nullable();
            $table->integer('priority_id');
            $table->integer('user_id');
            $table->string('attachment', 100)->nullable();
            $table->dateTime('open_date')->nullable();
            $table->integer('assignee')->nullable();
            $table->double('satisfaction')->nullable();
            $table->integer('ticket_status_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
