<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemSetting extends Model
{
    protected $table = 'system_settings';
    public $timestamps = false;

    protected $fillable = [
        'company_name', 'company_logo'
    ];
}
