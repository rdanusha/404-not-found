<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportTeam extends Model
{
    protected $table = 'support_team';
    public $timestamps = false;

    protected $fillable = [
        'name', 'status',
    ];

    public function teamMembers()
    {
        return $this->hasMany(SupportTeamMembers::class, 'team_id', 'id');
    }
}
