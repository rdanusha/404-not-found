<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

    protected $table = 'comments';
    public $timestamps = false;

    protected $fillable = [
        'chat_id', 'user_id', 'message', 'attachment', 'tickets_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'tickets_id', 'id');
    }
}
