<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    public $timestamps = false;

    protected $fillable = [
        'ticket_no', 'subject', 'details', 'priority_id', 'user_id', 'attachment', 'open_date',
        'assignee', 'satisfaction', 'ticket_status_id', 'ticket_topics_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee', 'id');
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class, 'priority_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(TicketStatus::class, 'ticket_status_id', 'id');
    }

    public function topic()
    {
        return $this->belongsTo(TicketTopic::class, 'ticket_topics_id', 'id');
    }
}
