<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportTeamMembers extends Model
{
    protected $table = 'support_team_members';
    public $timestamps = false;

    protected $fillable = [
        'team_head', 'user_id', 'team_id'
    ];

    public function team()
    {
        return $this->belongsTo(SupportTeam::class, 'team_id', 'id');
    }
}
