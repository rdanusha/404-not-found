<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketTopic extends Model
{
    protected $table = 'ticket_topics';
    public $timestamps = false;

    protected $fillable = [
        'topic', 'support_team_id', 'status'
    ];

    public function supportTeam()
    {
        return $this->belongsTo(SupportTeam::class,'support_team_id','id');
    }
}
